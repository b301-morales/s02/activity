package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[] args) {


        // USING ARRAY

        int[] primeArray = new int[5];

        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;


        System.out.println("--------------------------------------------");
        System.out.println("ARRAY METHOD");
        System.out.println("--------------------------------------------");
        System.out.println("The first prime number is:   "+ primeArray[0]);
        System.out.println("The second prime number is:  "+ primeArray[1]);
        System.out.println("The third prime number is:   "+ primeArray[2]);
        System.out.println("The fourth prime number is:  "+ primeArray[3]);
        System.out.println("The fifth prime number is:   "+ primeArray[4]);
        System.out.println("--------------------------------------------");
        System.out.println();



        // USING ARRAY LIST

        ArrayList<String> starWarsCharacters = new ArrayList<String>();
        starWarsCharacters.add("Luke");
        starWarsCharacters.add("Yoda");
        starWarsCharacters.add("Darth Vader");
        starWarsCharacters.add("Obi Wan");


        System.out.println("--------------------------------------------");
        System.out.println("ARRAY LIST METHOD");
        System.out.println("--------------------------------------------");
        System.out.println("Some commonly known Star Wars Characters are: " + starWarsCharacters);
        System.out.println("--------------------------------------------");
        System.out.println();

        // USING HASHMAPS

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("Mouse", 23);
        inventory.put("Keyboard", 41);
        inventory.put("Monitor", 12);

        System.out.println("--------------------------------------------");
        System.out.println("HASHMAP METHOD");
        System.out.println("--------------------------------------------");
        System.out.println("Our current inventory consists of : " + inventory);
        System.out.println("--------------------------------------------");
        System.out.println();
    }
}
