package com.zuitt;

import java.util.Scanner;

public class WDC043_S2_A1 {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);



        System.out.println();
        System.out.println("----------------------------");
        System.out.println("LEAP YEAR CHECKER.");
        System.out.println("----------------------------");
        System.out.println("Input");
        System.out.print("Year: ");
        int year = userInput.nextInt();

        if (((year % 4 == 0) &&
                !(year % 100 == 0))
                || (year % 400 == 0)){

            System.out.println();
            System.out.println(year + " is a LEAP year.");
        } else {
            System.out.println();
            System.out.println(year + " is NOT a leap year.");
        }
        System.out.println("----------------------------");
    }
}
